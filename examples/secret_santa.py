# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT
import asyncio
import random
import json
from itertools import pairwise
from typing import Optional, Sequence

from matrix_bot.bot import MatrixBot
from tchap_bot.config import env_config


EURO_AMOUNT = "[TBD]"
LIMIT_DATE = "[TBD]"
ROOM_NAME = "secret conversation with santa"
HANDLE_LIST = [
    "@jean.dupont-finances.gouv.fr:agent.finances.tchap.gouv.fr",
    "@marie.martin-finances.gouv.fr:agent.finances.tchap.gouv.fr",
]
SERVER_LIST = ["finances.gouv.fr:agent.finances.tchap.gouv.fr"]
MANUAL_HANDLE_TO_NAME = {"@jean.dupont-finances.gouv.frwtfisthis:agent.finances.tchap.gouv.fr": ["Jean", "Dupont"]}


def room_dump_to_handle_list(room_dump: str):
    return room_dump.split(":")


async def send_direct_message(
    matrix_bot: MatrixBot, message: str, user_to_invite: str, room_name: Optional[str] = None
):
    await matrix_bot.connect()
    room_creation = await matrix_bot.matrix_client.room_create(name=room_name, is_direct=True, invite=[user_to_invite])
    await matrix_bot.matrix_client.sync()
    await matrix_bot.matrix_client.send_text_message(room_creation.room_id, message)


def handle_to_name(handle: str, server_list: Sequence[str]):
    if handle in MANUAL_HANDLE_TO_NAME:
        return MANUAL_HANDLE_TO_NAME[handle]
    for server in server_list:
        handle = handle.removesuffix(f"-{server}").removeprefix("@")
    result = handle.split(".")
    if len(result) > 2:
        raise RuntimeError(
            f"It seems we weren't able to parse the name of {handle}, "
            "consider checking if his server is in SERVER_LIST or add him to MANUAL_HANDLE_TO_NAME"
        )
    return result


def main(server_list, handle_list):
    random.seed(a=int("1100110011100110101110", 2))
    name_dict = {handle: handle_to_name(handle, server_list) for handle in handle_list}
    secret_santa_sorted_list = sorted(handle_list, key=lambda x: random.random())
    secret_santa_dict = dict(pairwise(secret_santa_sorted_list + [secret_santa_sorted_list[0]]))
    with open("santa_trace.json", "w") as secret_santa_json:
        json.dump(secret_santa_dict, secret_santa_json)

    for giver_handle, receiver_handle in secret_santa_dict.items():
        giver_firstname, _giver_lastname = name_dict[giver_handle]
        receiver_firstname, receiver_lastname = name_dict[receiver_handle]
        message = f"""
Subject: Secret Santa du PEReN
Bonjour {giver_firstname} :)

La magie de Noël t'as désigné(e) en tant que secret Santa de {receiver_firstname} {receiver_lastname}.
Tu as donc jusqu'au {LIMIT_DATE} pour lui trouver un cadeau original, dans la limite des {EURO_AMOUNT} euros !

Joyeuses fêtes de fin d'année !
--
L'équipe du PEReNoël
        """
        matrix_bot = MatrixBot(
            env_config.matrix_home_server,
            env_config.matrix_bot_username,
            env_config.matrix_bot_password,
            use_functions=True,
        )
        asyncio.run(send_direct_message(matrix_bot, message, giver_handle, ROOM_NAME))


if __name__ == "__main__":
    # usage :
    # edit the constants on this file then
    # .venv/bin/python examples/secret_santa.py
    main(SERVER_LIST, HANDLE_LIST)
