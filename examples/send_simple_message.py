# SPDX-FileCopyrightText: 2021 - 2022 Isaac Beverly <https://github.com/imbev>
# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT
import asyncio
import sys
from tchap_bot.config import env_config
from tchap_bot.bot import MatrixBot


async def send_message(matrix_bot, room_id, message):
    await matrix_bot.connect()
    await matrix_bot.matrix_client.send_text_message(room_id, message)


if __name__ == "__main__":
    # usage .venv/bin/python examples/send_simple_message.py '[see_on_room_advanced_parameters].tchap.gouv.fr' "Bonjour"
    room_id, message = sys.argv[1], sys.argv[2]
    matrix_bot = MatrixBot(
        env_config.matrix_home_server,
        env_config.matrix_bot_username,
        env_config.matrix_bot_password,
        use_functions=True,
    )
    asyncio.run(send_message(matrix_bot, room_id, message))
