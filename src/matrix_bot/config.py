# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT
import logging
from pathlib import Path

import structlog
from nio.crypto import ENCRYPTION_ENABLED
from pydantic import Field
from pydantic_settings import BaseSettings, SettingsConfigDict


class BotLibConfig(BaseSettings):
    """
    A class to handle built-in user-configurable settings, including support for saving to and loading from a file.
    Can be inherited from by bot developers to implement custom settings.
    """

    timeout: int = Field(
        default=60_000,
        description="The maximum time that the server should wait for new events before "
        "it should return the request anyways, in milliseconds",
    )
    join_on_invite: bool = Field(
        default=False,
        description="Do the bot automatically join when invited. Ignored if allowed_room_ids is not empty.",
    )
    allowed_room_ids: list[str] = Field(
        default=[],
        description=(
            "List of allowed room IDs. All rooms are allowed if the list is empty. "
            "Rooms not in this list will be left and invites rejected."
        ),
    )
    encryption_enabled: bool = Field(default=ENCRYPTION_ENABLED)
    ignore_unverified_devices: bool = Field(default=True, description="True by default in Element")
    store_path: Path = Field(default="./store/", description="path in which matrix-nio store will be written")
    session_path: Path = Field(default="session.txt", description="path of the file to store session identifier")
    log_level: int = Field(default=logging.INFO, description="log level for the library")
    salt: bytes = Field(
        default=b"\xce,\xa1\xc6lY\x80\xe3X}\x91\xa60m\xa8N",
        description="Salt to store your session credentials. Should not change between two runs",
    )
    send_error_message: bool = Field(
        default=False,
        description="send an error message when message is not decrypted. "
        "DO NOT set to true if there are more than one bot in a room "
        "(it can lead to infinite messages exchanges between the two bots.",
    )

    model_config = SettingsConfigDict(env_file=Path(".matrix_bot_env"))


bot_lib_config = BotLibConfig()
structlog.configure(
    wrapper_class=structlog.make_filtering_bound_logger(bot_lib_config.log_level),
)
logger = structlog.get_logger("Matrix bot")
logger.info("starting the bot")
